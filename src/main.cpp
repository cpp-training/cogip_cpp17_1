#include <iostream>
#include <string>
#include <vector>

#include "structure_bindings.h"
#include "scoped_variables.h"
#include "auto_deduction.h"
#include "tpl_constexpr.h"
#include "fold_expressions.h"
#include "meta_intro.h"


template<typename T>
std::ostream& operator<<(std::ostream& os, const std::vector<T>& vec)
{
    for (auto& el : vec)
    {
        os << el << ' ';
    }
    return os;
}


int main()
{
    std::vector<std::string> vec = {
        "Hello", "from", "GCC", __VERSION__, "!" 
    };
    std::cout << vec << std::endl;

    // Using structured bindingsto unpack bundled values
    //structure_bindings();

    // limiting variable scopes to if and switch statements
    //scoped_variables_if_switch();

    // Automatic deduction of template class type
    //automatic_deduction();

    // Simplifying compile time decisions with constexpr-if
    //template_constexpr_if();

    // Template fold expressions
    //main_templates();

    // A slice of template meta-programming
    meta_programming_intro();


    return 1;
}
