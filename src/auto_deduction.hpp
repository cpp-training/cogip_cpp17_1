#include <iostream>
#include <type_traits>


#include "auto_deduction.h"



template <typename T1, typename T2, typename T3>
class MyWrapper {
    T1 t1;
    T2 t2;
    T3 t3;

public:
    explicit MyWrapper(T1 a_t1, T2 a_t2, T3 a_t3)
    : t1{a_t1}, t2{a_t2}, t3{a_t3}
    {
        std::cout << "Ctor MyWrapper" << std::endl;
    }

    void show_content()
    {
        std::cout << "t1: " << t1 << "\t\t"
                  << "t2: " << t2 << "\t\t"
                  << "t3: " << t3 << "\t\t" << std::endl;
    }


};




template <typename T>
struct Sum_t {
    // What type is T ?
    // if we provide int, float, and doubles 
    // the compiler needs to figure out which type fits all values ...
    T value;

    template <typename ... Ts>
    Sum_t(Ts&& ... values) :  value{(values + ...)} {}

};

// ... this job is done using a deduction guide C++17
template <typename ... Ts>
Sum_t(Ts&& ... values) -> Sum_t<std::common_type_t<Ts...>>;

