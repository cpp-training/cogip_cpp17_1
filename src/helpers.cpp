#include <iostream>

#include "helpers.h"


void pause()
{
    std::cout << std::endl << "Press any key to continue...";
    std::cin.get();
}

std::size_t stringLength(const char* source)
{
    if(source == nullptr) { return 0; }

    std::size_t length = 0;
    while(*source != '\0') {
        length++;
        source++;
    }
    return length;
}

/**
 * @brief getHash
 * https://codereview.stackexchange.com/questions/85556/simple-string-hashing-algorithm-implementation
 * @param source
 * @return
 */
std::size_t getHash(const char* source)
{
    std::size_t length = stringLength(source);
    std::size_t hash = 0;
    for(std::size_t i = 0; i < length; i++) {
        char c = source[i];
        int a = c - '0';
        hash = (hash * 10) + a;
    }

    return hash;
}


void banner(const std::string & message, transition type)
{
    switch(type)
    {
        case( transition::section ):
            std::cout << std::endl;
            std::cout << R"(/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\)" << std::endl;
            std::cout << R"(\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/)" << std::endl;
            std::cout << "                                          " << std::endl;
            std::cout << "###### " <<    message      << " ######## " << std::endl;
            std::cout << "                                          " << std::endl;
            std::cout << R"(/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\)" << std::endl;
            std::cout << R"(\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/)" << std::endl;
            std::cout << std::endl;
            break;

        case( transition::contrast ):
            std::cout << "+ - - + + - - + + - - + + - - + + - - +" << std::endl;
            std::cout << "                                       " << std::endl;
            std::cout << message << std::endl;
            std::cout << "                                       " << std::endl;
            std::cout <<   "+ - - + + - - + + - - + + - - + + - - +"   << std::endl;
            break;

        case( transition::message ):
            std::cout << "\n" << message ;
            std::cout <<   "-------------------------------------------------\n"   << std::endl;
            break;

        default:
            std::cout << "Invalid transition" << std::endl;
            break;
    }

}


std::string ascii_art_banner()
{
    std::string cpp17_banner;
    cpp17_banner += R"( ________  _______   ________ ________  ________  _______           ________  ________  ________         _____   ________  )" ;
    cpp17_banner += "\n";
    cpp17_banner += R"(|\   __  \|\  ___ \ |\  _____\\   __  \|\   __  \|\  ___ \         |\   ____\|\   __  \|\   __  \       / __  \ |\_____  \ )";
    cpp17_banner += "\n";
    cpp17_banner += R"(\ \  \|\ /\ \   __/|\ \  \__/\ \  \|\  \ \  \|\  \ \   __/|        \ \  \___|\ \  \|\  \ \  \|\  \     |\/_|\  \ \|___/  /|)";
    cpp17_banner += "\n";
    cpp17_banner += R"( \ \   __  \ \  \_|/_\ \   __\\ \  \\\  \ \   _  _\ \  \_|/__       \ \  \    \ \   ____\ \   ____\    \|/ \ \  \    /  / /)";
    cpp17_banner += "\n";
    cpp17_banner += R"(  \ \  \|\  \ \  \_|\ \ \  \_| \ \  \\\  \ \  \\  \\ \  \_|\ \       \ \  \____\ \  \___|\ \  \___|         \ \  \  /  / / )";
    cpp17_banner += "\n";
    cpp17_banner += R"(   \ \_______\ \_______\ \__\   \ \_______\ \__\\ _\\ \_______\       \ \_______\ \__\    \ \__\             \ \__\/__/ /  )";
    cpp17_banner += "\n";
    cpp17_banner += R"(    \|_______|\|_______|\|__|    \|_______|\|__|\|__|\|_______|        \|_______|\|__|     \|__|              \|__||__|/   )";


    return cpp17_banner;
}

