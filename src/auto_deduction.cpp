
#include <tuple>

#include "helpers.h"

#include "auto_deduction.h"
#include "auto_deduction.hpp"


void stl_example();

void mywrapper_example();

void deduction_guide();


void automatic_deduction()
{
    banner("ITEM : AUTOMATIC TYPE DEDUCTION", transition::section);

    stl_example();

    mywrapper_example();

    deduction_guide();

}


void stl_example()
{
    banner("==> Example: type deduction using STL built in types \n", transition::message);

    // C++ 17 way
    std::pair my_pair{123, "abc"};
    std::tuple my_tuple{64, 12.3, 'c',"winter is coming"};

    // C++ 14 old fashionned style
    std::pair<int, const char *> my14_pair{123, "abc"};
    std::tuple<int, double, char, const char *> my14_tuple{64, 12.3, 'c', "winter is coming"};
}

void mywrapper_example()
{
    banner("==> Example: type deduction with Custom class \n", transition::message);

    // Implicit template type deduction
    // C++ 17 way
    MyWrapper wrap01 {123, 1.23, "abc"};

    // C++ 14 old fashionned style
    MyWrapper<const char *, double, const char *> wrap02 {"toto", 877897897897, "abc"};

    wrap01.show_content();

    wrap02.show_content();
}


void deduction_guide()
{
    banner("==> Example: Add a deduction guide to your variadic template \n", transition::message);

    // Add explicit deduction guide to deduce the result type
    Sum_t s {1u, 2.0, 3, 4.0f};
    Sum_t string_sum {std::string{"abc"}, "def"};

    std::cout << s.value << std::endl;

    std::cout << string_sum.value << std::endl;
}
