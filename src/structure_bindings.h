#ifndef STRUCTURE_BINDINGS_H
#define STRUCTURE_BINDINGS_H

#include <map>
#include <cstdlib>

#include "helpers.h"

void structure_bindings();

std::pair<int, int> divide_remainder(int num, int denom);

struct Employee{
    uint32_t m_id;
    std::string m_name;
    std::string m_role;
    uint32_t m_salary;

    Employee(std::string name, std::string role)
        : m_name{name}, m_role{role}
    {
        m_salary = 2500 + std::rand()/(RAND_MAX/1000);

        m_id = static_cast<uint32_t>(getHash(m_name.c_str()));
    }

    Employee(std::pair<std::string, std::string> employee)
        : Employee(employee.first, employee.second)
    {}

};


#endif // STRUCTURE_BINDINGS_H
