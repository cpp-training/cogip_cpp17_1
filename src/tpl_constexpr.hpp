#ifndef TPL_CONSTEXPR_HPP
#define TPL_CONSTEXPR_HPP

#include <type_traits>
#include <vector>

template <typename T>
class addable17
{
    T val;
public:
    addable17(T v) : val{v} {}

    const T & value() const
    {
        return val;
    }

    template <typename U>
    T add(U x) const 
    {
        if constexpr (std::is_same_v<T, std::vector<U>>) 
        {
            auto copy (val);
            for (auto &n : copy) {
                n += x;
            }
            std::cout << "Add to each element" << std::endl;

            return copy;
        } 
        else 
        {
            // Simple scalar addition 
            // or string concat depending on type

            return val + x;
        }
    }
};


template <typename T>
class addable14
{
    T val;
public:
    addable14(T v) : val{v} {}

    const T & value() const
    {
        return val;
    }

    // SFINAE: Substitution Failure is not an Error :)
    // compiler might not deduce return type for this function
    template <typename U>
    std::enable_if_t<!std::is_same<T, std::vector<U>>::value, T>
    add(U x) const { return val + x; }

    // SFINAE: Substitution Failure is not an Error :)
    // compiler might not deduce return type for this function
    template <typename U>
    std::enable_if_t<std::is_same<T, std::vector<U>>::value, std::vector<U>>
    add(U x) const {
        auto copy (val);
        for (auto &n : copy) 
        {
            n += x;
        }

        return copy;
    }
};


#endif // TPL_CONSTEXPR_HPP
