#include <string>
#include <iostream>
#include <vector>

#include "helpers.h"

#include "tpl_constexpr.hpp"


void addable_cpp17();
void addable_cpp14();

// Easy peasy template
template<typename T>
std::ostream& operator<<(std::ostream& os, const std::vector<T>& vec)
{
    for (auto& el : vec)
    {
        os << el << ' ';
    }
    return os;
}



void  template_constexpr_if()
{

    banner("ITEM : AUTOMATIC TYPE DEDUCTION", transition::section);

    addable_cpp17();

    addable_cpp14();

}


void addable_cpp14()
{
    banner("==> Example: C++14 sibling code - SFINAE solution\n", transition::message);

    std::cout << "The following code " << std::endl
              << R"(addable14<std::string>{"aa"}.add("bb"))" << std::endl
              << "just concatanates strings: " 
              << addable14<std::string>{"aa"}.add("bb") << std::endl;    // is "aabb"

    std::cout << std::endl;
    std::vector<int> v {1, 2, 3};

    std::cout << std::endl;
    addable14 vec01{v};  
    std::cout << "Before add, vec01.value() is: " << vec01.value() << std::endl;
    std::cout << "The following code " << std::endl
              << R"(auto vec01_a = vec01.add(10);)" << std::endl;
    auto vec01_a = vec01.add(10);   // is std::vector<int>{11, 12, 13}
    std::cout << "After add, vec01_a is: " << vec01_a << std::endl;

    std::cout << std::endl;
    std::vector<std::string> sv {"a", "b", "c"};

    std::cout << std::endl;
    addable14<std::vector<std::string>> sv_copy{sv};   // is {"az", "bz", "cz"}
    std::cout << "Before add, sv_copy.value() is: " << sv_copy.value() << std::endl;
    std::cout << "The following code " << std::endl
              << R"(auto sv_a = sv_copy.add(std::string{"_dice_are_rolling"});)" << std::endl;
    auto sv_a = sv_copy.add(std::string{"_dice_are_rolling"});
    std::cout << "After add, sv_a is: " << sv_a << std::endl;
}



void addable_cpp17()
{
    banner("==> Example: C++17 constexpr-if compile time decision\n", transition::message);

    addable17<int>{1}.add(2);    // is 3
    addable17<float>{1.0}.add(2.0); // is 3.0

    std::cout << "The following code " << std::endl
              << R"(addable17<std::string>{"aa"}.add("bb"))" << std::endl
              << "just concatanates strings: " 
              << addable17<std::string>{"aa"}.add("bb") << std::endl;    // is "aabb"

    std::cout << std::endl;
    std::vector<int> v {1, 2, 3};

    std::cout << std::endl;
    addable17 vec01{v};  
    std::cout << "Before add, vec01.value() is: " << vec01.value() << std::endl;
    std::cout << "The following code " << std::endl
              << R"(auto vec01_a = vec01.add(10);)" << std::endl;
    auto vec01_a = vec01.add(10);   // is std::vector<int>{11, 12, 13}
    std::cout << "After add, vec01_a is: " << vec01_a << std::endl;

    std::cout << std::endl;
    std::vector<std::string> sv {"a", "b", "c"};

    std::cout << std::endl;
    addable17<std::vector<std::string>> sv_copy{sv};   // is {"az", "bz", "cz"}
    std::cout << "Before add, sv_copy.value() is: " << sv_copy.value() << std::endl;
    std::cout << "The following code " << std::endl
              << R"(auto sv_a = sv_copy.add(std::string{"_dice_are_rolling"});)" << std::endl;
    auto sv_a = sv_copy.add(std::string{"_dice_are_rolling"});
    std::cout << "After add, sv_a is: " << sv_a << std::endl;


}


