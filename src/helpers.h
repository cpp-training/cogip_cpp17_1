#ifndef HELPERS_H
#define HELPERS_H

#include <cstdint>
#include <string>

enum class transition
{
    section=0,
    contrast, 
    message
};


std::size_t stringLength(const char* source);
std::size_t getHash(const char* source);

void pause();

std::string ascii_art_banner();

void banner(const std::string & message, transition type);

#endif // HELPERS_H
