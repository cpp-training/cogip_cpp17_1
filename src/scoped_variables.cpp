#include <iostream>
#include <map>
#include <stdexcept>

#include "helpers.h"

#include "scoped_variables.h"


static const std::multimap<char, std::string> discover_the_zoo{
    {'a', "anaconda"},
    {'b', "baboon"}, {'b', "badger"}, 
    {'c', "camel"}, {'c', "capucin"}, {'c', "caracal"},
    {'k', "kangaroo"}, {'k', "komodo dragon"}
};


void scoped_variables_if_switch()
{
    banner("ITEM : LIMITING VARIABLE SCOPES TO IF AND SWITCH STATEMENTS", transition::section);

    scoped_if();

    pause();

    scoped_switch();

    pause();

}


void scoped_if()
{
    find_in_dico('c');

    find_in_dico('e');

    find_in_dico('k');

}


void find_in_dico(const char letter)
{
    std::cout << "Key: " << letter << std::endl;

    if(auto itr{discover_the_zoo.find(letter)}; itr != discover_the_zoo.end())
    {
        std::cout << "Values: " <<std::endl;
        for(auto itr=discover_the_zoo.lower_bound(letter) ; itr != discover_the_zoo.upper_bound(letter) ; itr++)
        {
            std::cout << itr->second << ",\t";
        }
        std::cout << std::endl;
    } 
    else 
    {
        std::cout << "ERROR: Unknown Key !"<< std::endl;
    }
}



void scoped_switch()
{
    keyboard_diag();

}


enum class key_status{
    trapped=0,
    valid
};

static std::map<char, key_status> trapped_keys{
    {'a', key_status::valid}, {'z', key_status::valid}, {'e', key_status::valid},
    {'r', key_status::valid}, {'t', key_status::valid}, {'y', key_status::valid}
};

void keyboard_diag()
{
    bool condition = true;

    do 
    {
        std::cout << "PRESS ANY KEY" << std::endl;

        char letter; 
        std::cin >> letter;

        try { 
            auto status = trapped_keys.at(letter);
        } 
        catch (const  std::out_of_range & e) { 
            trapped_keys.insert({letter, key_status::trapped});
        }

        switch(auto selector = trapped_keys.at(letter)) {
            case key_status::valid:
                std::cout << "VALID Key pressed: " << letter << std::endl;
                break;

            default:
                std::cout << "TRAPPED KEY" << std::endl;
                condition = false;
                break;
        }
    } while(condition);

}

