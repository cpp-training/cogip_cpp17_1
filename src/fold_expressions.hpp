#ifndef FOLD_EXPRESSIONS_HPP
#define FOLD_EXPRESSIONS_HPP

template <typename ... Ts>
auto sum(Ts ... ts);

#endif // FOLD_EXPRESSIONS_HPP
