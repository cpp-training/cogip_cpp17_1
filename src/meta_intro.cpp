#include <algorithm>
#include <iostream>
#include <set>
#include <string>
#include <type_traits>
#include <vector>

#include "helpers.h"
#include "type_deducer.hpp"


#include "meta_intro.h"


// Easy peasy template
template<typename T>
std::ostream& operator<<(std::ostream& os, const std::set<T>& vec)
{
    for (auto& el : vec)
    {
        os << el << ' ';
    }
    return os;
}


// https://stackoverflow.com/questions/17854219/creating-a-sub-tuple-starting-from-a-stdtuplesome-types
template<typename... T, std::size_t... I>
auto subtuple_(const std::tuple<T...>& t, std::index_sequence<I...>)
{
    return std::make_tuple(std::get<I>(t)...);
}

template<int Trim, typename... T>
auto subtuple(const std::tuple<T...>& t)
{
    return subtuple_(t, std::make_index_sequence<sizeof...(T) - Trim>());
}

// Look at ??
// https://stackoverflow.com/questions/20162903/template-parameter-packs-access-nth-type-and-nth-element

// Helper structure which checks the types of the values and make sure that they 
// are strictly the sames as the first one
template<typename T, typename... Ts>
struct are_same : std::conjunction<std::is_same<T, Ts>...>
{
};


// C++14 variadic template
// adder tail function
template<typename T>
std::string type_list(T v)
{
    return type_name<T>();
}

// C++14 variadic template
// adder core function
template<typename T, typename... Args>
std::string type_list(T first, Args... args)
{
    return type_name<decltype(first)>() + std::string("\n") + type_list(args...);
}

// C++17 unary fold template
// this function inserts an arbitrary number of variadic parameters
// into a std::set
template<typename stype, typename... Ts>
bool insert_set_strict(std::set<stype>& set, Ts... ts)
{
    if constexpr (are_same<stype, Ts...>::value)
    {
        std::cout << "Values are the *strictly* the same as the set internal type !\n";
        return (set.insert(ts).second && ...);
    }
    else
    {
        std::cout << "--------- ERROR ---------" << std::endl;
        std::cout << "std::set typename is : " << type_name<stype>() << std::endl;
        std::cout << "Variadic pack types are : \n" << type_list(ts...) << std::endl;
        std::cout << "-------------------------" << std::endl;

        return false;
    }
}

// Ensures that types are convertible to the first type
template<typename T, typename... Ts>
struct are_convertible : std::conjunction<std::is_convertible<T, Ts>...>
{
};

template<typename stype, typename... Ts>
bool insert_set_convertible(std::set<stype>& set, Ts... ts)
{
    //if constexpr (are_convertible<stype, Ts...>::value)
    // forms the logical of the type traits Ts...
    if constexpr (std::conjunction<std::is_convertible<stype, Ts>...>::value)
    {
        std::cout << "Values are convertible to the set internal type !\n";
        return (set.insert(ts).second && ...);
    }
    else
    {
        std::cout << "--------- ERROR ---------" << std::endl;
        std::cout << "std::set typename is : " << type_name<stype>() << std::endl;
        std::cout << "Variadic pack types are : \n" << type_list(ts...) << std::endl;
        std::cout << "-------------------------" << std::endl;

        return false;
    }
}



void meta_programming_intro()
{
    banner("BONUS ITEM : INTRO TO META-PROGRAMMING", transition::section);

    insertion_into_set_example();

    pause();
}


void insertion_into_set_example()
{
    std::set<int> initial_set{ 1, 2, 3 };
    std::set<int> my_set{ 1, 2, 3 };

    std::cout << "my_set = " << my_set << std::endl;

    banner("==> C++17 Example: multiple insertions WITH STRICT VERIFICATION of types :) \n",
           transition::message);

    {
        my_set = initial_set;

        std::cout << R"(insert_set_strict(my_set, 7, 2.5, 7.0f, 125.0)" << std::endl;
        auto sucess = insert_set_strict(my_set, 7, 2.5, 7.0f, 125.0); 

        std::cout << "Success ? " << std::boolalpha << sucess << std::endl;

        for (auto&& elem : my_set)
        {
            std::cout << elem << " ";
        }
        std::cout << std::endl;
    }
    pause();

    banner("==> C++17 Example: multiple insertions WITH DIFFERENT CONVERTIBLE types :) \n",
           transition::message);

    {
        my_set = initial_set;

        std::cout << R"(insert_set_convertible(my_set, 9, 12.5, 13.2f) )" << std::endl;
        auto sucess
            = insert_set_convertible(my_set, 9, 12.5, 13.2f); 

        std::cout << "Success ? " << std::boolalpha << sucess << std::endl;

        for (auto&& elem : my_set)
        {
            std::cout << elem << " ";
        }
        std::cout << std::endl;
    }
    pause();

    banner("==> C++17 Example: multiple insertions WITH NON CONVERTIBLE types :) \n",
           transition::message);

    {
        my_set = initial_set;

        std::cout << R"(insert_set_convertible(my_set, 9, 12.5, 13.2f, std::string("bidule")) )" << std::endl;
        auto sucess
            = insert_set_convertible(my_set, 9, 12.5, 13.2f, std::string("bidule")); 

        std::cout << "Success ? " << std::boolalpha << sucess << std::endl;

        for (auto&& elem : my_set)
        {
            std::cout << elem << " ";
        }
        std::cout << std::endl;
    }
    pause();


}


