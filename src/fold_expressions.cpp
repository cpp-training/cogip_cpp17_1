#include <algorithm>
#include <iostream>
#include <set>
#include <string>
#include <vector>

#include "fold_expressions.h"
#include "helpers.h"

#include "fold_expressions.hpp"
#include "type_deducer.hpp"

// Easy peasy template
template<typename T>
std::ostream& operator<<(std::ostream& os, const std::vector<T>& vec)
{
    for (auto& el : vec)
    {
        os << el << ' ';
    }
    return os;
}

// Easy peasy template
template<typename T>
std::ostream& operator<<(std::ostream& os, const std::set<T>& vec)
{
    for (auto& el : vec)
    {
        os << el << ' ';
    }
    return os;
}

// C++17 binary fold template
template<typename... Ts>
auto sum_num(Ts... ts)
{
    return (ts + ... + 0);
}

// C++17 unary fold template
template<typename... Ts>
auto sum_other(Ts... ts)
{
    return (ts + ...);
}

// C++14 variadic template
// adder tail function
template<typename T>
T adder(T v)
{
    return v;
}

// C++14 variadic template
// adder core function
template<typename T, typename... Args>
T adder(T first, Args... args)
{
    return first + adder(args...);
}

// C++17 unary fold template
// It returns the total count of elements in the range
template<typename R, typename... Ts>
auto matches(const R& range, Ts... ts)
{
    return (std::count(std::begin(range), std::end(range), ts) + ...);
}

// C++17 unary fold template
// this function inserts an arbitrary number of variadic parameters
// into a container
template<typename T, typename... Ts>
bool insert_all(T& container, Ts... ts)
{
    // what if container has no insert method ?
    return (container.insert(ts).second && ...);
}

// C++17 unary fold template
// this function inserts an arbitrary number of variadic parameters
// into a std::set
template<typename stype, typename... Ts>
bool insert_set_simple(std::set<stype>& set, Ts... ts)
{
    // what if Ts type is not compatible with stype ?
    return (set.insert(ts).second && ...);

    /* https://en.cppreference.com/w/cpp/container/set/insert */
    // insert returns a std::pair<iterator, bool>
}



void main_templates()
{
    banner("ITEM : FOLD EXPRESSIONS", transition::section);

    fold_expressions();

    pause();

    variadic_cpp14();

    pause();

    find_matches();

    pause();

    set_insertions();
}

void fold_expressions()
{
    banner("==> C++17 Example: binary fold template \n", transition::message);

    std::cout << "sum(1,2,3,4,5) = " << sum_num(1, 2, 3, 4, 5) << std::endl;

    std::cout << "sum() = " << sum_num() << std::endl;

    banner("==> C++17 Example: unary fold template \n", transition::message);

    std::string str_a{ "Hello " };
    std::string str_b{ "World" };
    std::cout << "sum(str_a, str_b) = " << sum_other(str_a, str_b) << std::endl;
}

void variadic_cpp14()
{
    banner("--> Let us examine C++ 14 solution", transition::contrast);

    std::cout << "adder(1, 2, 3, 8, 7) = " << adder(1, 2, 3, 8, 7) << std::endl;

    std::string s1 = "x", s2 = "aa", s3 = "bb", s4 = "yy";
    std::cout << s1 << " + " << s2 << " + " << s3 << " + " << s4 << " = ";
    std::cout << adder(s1, s2, s3, s4) << std::endl;
}

void find_matches()
{
    std::vector<int> v{ 1, 2, 3, 4, 5 };

    std::cout << "Vector v = " << v << std::endl;

    banner("==> C++17 Example: find variadic parameters in a std::vector<int> \n",
           transition::message);

    std::cout << "matches(v, 2, 5) = " << matches(v, 2, 5) << std::endl;
    std::cout << "matches(v, 100, 200) = " << matches(v, 100, 200) << std::endl;

    std::cout << R"(matches("abcdefg", 'x', 'y', 'z') = )" << matches("abcdefg", 'x', 'y', 'z')
        << std::endl;

    std::cout << R"(matches("abcdefg", 'a', 'd', 'f') = )" << matches("abcdefg", 'a', 'd', 'f')
        << std::endl;
}

void set_insertions()
{
    std::set<int> initial_set{ 1, 2, 3 };
    std::set<int> my_set{ 1, 2, 3 };

    std::cout << "my_set = " << my_set << std::endl;

    banner("==> C++17 Example: check if multiple insertions into a set are successful \n",
           transition::message);

    {
        std::cout << "insert_set_simple(my_set, 4, 5, 6)" << std::endl;
        auto sucess = insert_set_simple(my_set, 4, 5, 6); // Returns true

        std::cout << "Success ? " << std::boolalpha << sucess << std::endl;

        for (auto&& elem : my_set)
        {
            std::cout << elem << " ";
        }
        std::cout << std::endl;
    }
    pause();

    banner("==> C++17 Example: multiple insertions with no custom verification :) \n",
           transition::message);

    {
        my_set = initial_set;

        std::cout << "insert_set_simple(my_set, 7, 12.384, 8)" << std::endl;
        auto sucess = insert_set_simple(my_set, 7, 12.384, 8); 

        std::cout << "Success ? " << std::boolalpha << sucess << std::endl;

        for (auto&& elem : my_set)
        {
            std::cout << elem << " ";
        }
        std::cout << std::endl;
    }
    pause();


    // banner("==> C++17 Example: multiple insertions with no custom verification :) \n",
    //        transition::message);
    // {
    //     my_set = initial_set;
    //
    /* ----------------- ====> DOES NOT COMPILE    !!! <===== ----------------*/
    //     auto sucess = insert_set_simple(my_set, 7, std::string("toto"), 8); // Returns false, because the 2 collides

    //     std::cout << std::endl << R"(insert_set_simple(my_set, 7, "toto", 8))" << std::endl;
    //     std::cout << "Success ? " << std::boolalpha << sucess << std::endl;

    //     for (auto&& elem : my_set)
    //     {
    //         std::cout << elem << " ";
    //     }
    //     std::cout << std::endl;
    // }
    // pause();

 
}
