#include <iostream>
#include <string>
#include <cinttypes>
#include <map>
#include <vector>


#include "structure_bindings.h"

#include "type_deducer.hpp"

void std_pair_case();
void struct_case();
void before_cpp17();


// template <typename T> std::string type_name();

void structure_bindings()
{
    banner("ITEM : STRUCTURE BINDINGS", transition::section);

    std::map<int, bool> activation_map{
        {1, true}, {2, true}, {3, true},
        {4, true}, {5, true}, {6, true},
    };

    std_pair_case();

    pause();

    struct_case();

    pause();

    before_cpp17();

}

void std_pair_case()
{
    banner("==> Example: structure binding with - std::pair<T,T> \n", transition::message);

    const auto result = divide_remainder(16,3);

    std::cout << "Result is of type: " << type_name<decltype(result)>() << std::endl;

    std::cout << "16 / 3 is "
              << result.first << " with a remainder of "
              << result.second << std::endl;

    auto [fraction, remainder] = divide_remainder(112, 11);
    std::cout << "112 / 11 is "
              << fraction << " with a remainder of "
              << remainder << std::endl;
}


void struct_case()
{
    banner("==> Example: structure binding with - std::map<std::string, std::string> \n", transition::message);

    const std::map<std::string, std::string> collection{
        {"georges","dev"},
        {"lisa","dev"},
        {"etienne","CEO"},
        {"Nicolas","Software architect"},
        {"Ravi","Head of ML team"},
        {"mathieu","dev"},
        {"Aurélien","Manager"},
        {"Gary","intern"},
        {"Victor","dev"}
    };

    std::cout << "Collection contains" << std::endl;
    for(const auto & [name, role]: collection)
    {
        std::cout << "Name: " << name << std::endl;
        std::cout << "Role: " << role << std::endl;
    }

    pause();

    std::vector<Employee> registry;

    for(auto && example: collection)
    {
        registry.push_back(Employee{example});
    }

    banner("==> Example: structure binding with - std::vector<Employee> \n", transition::message);

    for(const auto &[id, name, role, salary]: registry)
    {
        std::cout << "Name: " << name << "\t"
                  << "Role: " << role << "\t\t"
                  << "Salary: " << salary << "\t\t"
                  << "[Unique id: "<< id << "]" << std::endl;
    }

    pause();

    {
        auto [id, name, role, salary] = registry[0];
        std::cout << name << "\t" << role << std::endl;
    }

    // does not compile !
    // Because registry[1] returns 4 members
    // Error: only 2 names provided for structured binding
    {
        // auto [id, name] = registry[1];
    }

}

void before_cpp17()
{
    banner("--> How can we mimic structure binding with C++14 ?", transition::contrast);

    banner("You can use std::tie<T1, T2>", transition::message);

    int remainder;

    std::tie(std::ignore, remainder) = divide_remainder(16, 5);
    std::cout << "Remainder of: 16 / 5 is " << remainder << '\n';

}



std::pair<int, int> divide_remainder(int num, int denom)
{

    return std::make_pair(num/denom, num%denom);
}

