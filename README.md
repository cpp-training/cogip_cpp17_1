# Short introduction to C++17 novelties



## Prerequesites
You will need to upgrade your compiler to a recent enough version.  
Updating CMake is also required.

Install gcc7 on Ubuntu 16.04:
- [follow this link](https://gist.github.com/jlblancoc/99521194aba975286c80f93e47966dc5)

Note:
You don't need to change the behavior of default symbolic links `gcc`
and `g++` to force pointing on `gcc7`.
See next section about compiler selection.

Update to cmake 3.16.0:
- [download the adequate archive](https://cmake.org/download/)
- [replace existing cmake - ubuntu 16.04](https://peshmerge.io/how-to-install-cmake-3-11-0-on-ubuntu-16-04/)

## Select the adequate compiler
If `gcc-7` is not your defeault compiler, you should create in your home directory a `conf-gcc7.cmake` file with the following content:
```cmake
set(CMAKE_C_COMPILER "/usr/bin/gcc-7" CACHE STRING "gcc compiler" FORCE)
set(CMAKE_CXX_COMPILER "/usr/bin/g++-7" CACHE STRING "g++ compiler" FORCE)

set(CERES_ROOT_DIR path_to_ceres)
```

Next, you just have to compile your project in your build directory
with a command like:
```shell
cmake -C ~/conf-gcc7.cmake ../cogip_cpp17_1
```


## Summary of new library and language features

A cheatsheet of [modern C++ language and library features](https://github.com/AnthonyCalandra/modern-cpp-features) by Anthony Calandra
